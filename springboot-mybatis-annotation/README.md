## 前言

学习总结，智慧的结晶，交流技术，不足之处请斧正。

springboot mybatis 注解版demo

## 技术选型

| 技术                   | 版本   | 说明                                    |
| ---------------------- | ------ | --------------------------------------- |
| Spring Boot            | 2.3.2  | MVC核心框架                             |
| MyBatis                | 3.5.5  | ORM框架                                 |
| mysql                 | 5.1.39 | mysql驱动                        |
| Swagger-UI             | 2.9.2  | 文档生产工具                            |
| hikari                 | 3.4.5  | 最快数据库连接池                            |
| log4j2                 | 2.11.2 | log日志                       |
| lombok                 | 1.18.0 | 简化书写代码                        |



## swagger地址

http://localhost:8080/swagger-ui.html


## 你的点赞鼓励，是我们前进的动力~
