package org.spring.springboot.service.impl;

import java.util.Date;
import java.util.List;

import org.spring.springboot.domain.Menu;
import org.spring.springboot.exception.APIException;
import org.spring.springboot.mapper.MenuMapper;
import org.spring.springboot.param.MenuAddParam;
import org.spring.springboot.param.MenuSearchParam;
import org.spring.springboot.param.MenuUpdParam;
import org.spring.springboot.res.ResultCode;
import org.spring.springboot.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuMapper menuMapper;

    public Menu get(Long id) {
    	return menuMapper.findById(id);
    }
    public Menu save(MenuAddParam param) {
    	Menu menu=new Menu();
    	menu.setIndex(param.getIndex());
    	menu.setName(param.getName());
    	menu.setParentId(param.getParentId());
    	menu.setUrl(param.getUrl());
    	menu.setCreateTime(new Date());
    	menuMapper.save(menu);
    	return menu;
    }
    
    public Menu update(MenuUpdParam param) {
    	Menu menu =this.menuMapper.findById(param.getId());
    	if(null==menu) {
    	  throw new APIException(ResultCode.FAILED,"无效的菜单编号");	
    	}    	
		 if(null!=param.getIndex()) {
	      menu.setIndex(param.getIndex());
		 }
    	 if(!StringUtils.isEmpty(param.getName())) {
    	  menu.setName(param.getName());
    	 }
    	 if(null!=param.getIndex()) {
    	  menu.setParentId(param.getParentId());
    	 }
    	 if(!StringUtils.isEmpty(param.getName())) {
    	  menu.setUrl(param.getUrl());
    	 }
    	 menuMapper.update(menu);
    	    	
    	return menu;
    }
    
    public int delete(Long id) {
       return menuMapper.deleteById(id);	
    }
	public List<Menu> getList(MenuSearchParam param) {
		return menuMapper.getList(param);
	}

	public List<Menu> getSubList() {
		return menuMapper.getSubList();
	}

}
