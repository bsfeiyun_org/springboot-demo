package org.spring.springboot.service;

import java.util.List;
import org.spring.springboot.domain.Menu;
import org.spring.springboot.exception.DemoException;
import org.spring.springboot.param.MenuAddParam;
import org.spring.springboot.param.MenuSearchParam;
import org.spring.springboot.param.MenuUpdParam;

public interface MenuService {
	Menu get(Long id);
	Menu save(MenuAddParam param);
	Menu update(MenuUpdParam param);
	int delete(Long id);
	
	List<Menu> getList(MenuSearchParam param);

	List<Menu> getSubList();
	
	
}
