package org.spring.springboot.service.impl;

import java.util.List;

import org.spring.springboot.domain.City;
import org.spring.springboot.mapper.CityMapper;
import org.spring.springboot.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 城市业务逻辑实现类
 */
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityMapper cityMapper;

    public City findCityByName(String cityName) {
    	List<City> list=cityMapper.findByName(cityName);
    	if(null!=list) {
    		return list.get(0);
    	}
        return null;
    }

}
