package org.spring.springboot.exception.handler;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spring.springboot.exception.APIException;
import org.spring.springboot.res.ResultCode;
import org.spring.springboot.res.ResultData;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

/**
 * 全局异常处理器
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 处理自定义异常
     */
	@ExceptionHandler(APIException.class)
	public ResultData handleException(APIException e) {
        // 打印异常信息
        log.error("### 异常信息:{} ###", e.getMessage());    
        if(e.getCode()==ResultCode.PERMISSION_TOKEN_EXPIRED) {
        	return new ResultData(ResultCode.LOGIN_AGAIN,"",e.getMsg());	
        }
        return new ResultData(ResultCode.VALIDATE_FAILED,"",e.getMsg());
	}

    /**
     * 自定义验证异常
     */
    /*@ExceptionHandler(BindException.class)
    public ResultVO validatedBindException(BindException e) {
    
        log.error(e.getMessage(), e);
        String message = e.getAllErrors().get(0).getDefaultMessage();        
        return new ResultVO(ResultCode.VALIDATE_FAILED,"",message);
    }
    */
	
    /**
     * 参数错误异常
     */
    @ExceptionHandler({MethodArgumentNotValidException.class, BindException.class})
    public ResultData handleException(Exception e) {

        if (e instanceof MethodArgumentNotValidException) {
            MethodArgumentNotValidException validException = (MethodArgumentNotValidException) e;
            BindingResult result = validException.getBindingResult();
            StringBuffer errorMsg = new StringBuffer();
            if (result.hasErrors()) {
                List<ObjectError> errors = result.getAllErrors();
                errors.forEach(p ->{
                    FieldError fieldError = (FieldError) p;
                    errorMsg.append(fieldError.getDefaultMessage()).append(",");
                    log.error("### 请求参数错误：{"+fieldError.getObjectName()+"},field{"+fieldError.getField()+ "},errorMessage{"+fieldError.getDefaultMessage()+"}"); 
                   }
                ); 
                for(ObjectError error:errors) {
                 return new ResultData(ResultCode.FAILED,"",error.getDefaultMessage());
                }
            }
        } else if (e instanceof BindException) {
            BindException bindException = (BindException)e;
            if (bindException.hasErrors()) {
                log.error("### 请求参数错误: {}", bindException.getAllErrors());                
            }
        }
        return new ResultData(ResultCode.FAILED,"",ResultCode.VALIDATE_FAILED.getMsg());
    }

    /**
     * 处理所有不可知的异常
     */
    @ExceptionHandler(Exception.class)
    public ResultData handleOtherException(Exception e){
        //打印异常堆栈信息
        e.printStackTrace();
        // 打印异常信息
        log.error("### 不可知的异常:{} ###", e.getMessage());        
        return new ResultData(ResultCode.FAILED,"",ResultCode.ERROR.getMsg());
    }

}
