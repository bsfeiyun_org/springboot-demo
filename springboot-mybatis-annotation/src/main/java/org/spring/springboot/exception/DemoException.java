package org.spring.springboot.exception;

import lombok.Getter;

@Getter
public class DemoException extends Exception {

	private static final long serialVersionUID = 7525258143798609878L;
	
	private int code;
    private String msg;

    public DemoException() {
        this(1001, "接口错误");
    }

    public DemoException(String msg) {
        this(1001, msg);
    }

    public DemoException(int code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }
}
