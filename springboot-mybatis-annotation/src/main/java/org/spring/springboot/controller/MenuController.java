package org.spring.springboot.controller;

import org.spring.springboot.param.MenuAddParam;
import org.spring.springboot.param.MenuSearchParam;
import org.spring.springboot.param.MenuUpdParam;
import org.spring.springboot.res.ResultData;
import org.spring.springboot.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="菜单接口")
@Controller
@RequestMapping(value = "/api/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;
    
    @ApiOperation(value="查询单个菜单信息", notes="根据菜单编号查询菜单信息")
    @RequestMapping( method = RequestMethod.POST)
    @ResponseBody
    public ResultData get(@RequestParam(required = true) Long id) {        
        return new ResultData(menuService.get(id));
    }
    
    @ApiOperation(value="新增菜单信息", notes="新增菜单信息")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseBody
    public ResultData create(@Validated@RequestBody MenuAddParam param) {
        return new ResultData(menuService.save(param));
    }
    
    @ApiOperation(value="更新菜单信息", notes="根据菜单编号更新菜单信息")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultData update(@Validated@RequestBody MenuUpdParam param){
        return new ResultData(menuService.update(param));
    }
    
    @ApiOperation(value="删除菜单信息", notes="根据菜单编号删除菜单信息")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public ResultData delete(@RequestParam(required = true) Long id) {
         menuService.delete(id);
        return new ResultData("");
    }
    
    @ApiOperation(value="查询菜单信息", notes="根据条件查询信息")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public ResultData getList(@RequestBody MenuSearchParam param) {
        return new ResultData(menuService.getList(param));
    }
    
    @ApiOperation(value="查询菜单树结构信息", notes="返回树结构")
    @RequestMapping(value = "tree", method = RequestMethod.GET)
    @ResponseBody
    public ResultData getSubList() {
        return new ResultData(menuService.getSubList());
    }
    
    
}
