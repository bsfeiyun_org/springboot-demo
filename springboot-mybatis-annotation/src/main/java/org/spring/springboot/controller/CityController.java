package org.spring.springboot.controller;

import org.spring.springboot.res.ResultData;
import org.spring.springboot.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="城市接口")
@Controller
@RequestMapping(value = "/api/city")
public class CityController {

    @Autowired    
    private CityService cityService;

    @RequestMapping( method = RequestMethod.GET)
    @ApiOperation(value="查询城市信息", notes="根据城市名称查询信息")
    @ResponseBody
    public ResultData get(@RequestParam(value = "cityName", required = true) String cityName) {        
        return new ResultData(cityService.findCityByName(cityName));
    }

}
