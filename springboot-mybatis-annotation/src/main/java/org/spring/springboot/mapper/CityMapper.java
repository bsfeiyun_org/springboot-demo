package org.spring.springboot.mapper;

import java.util.List;

import org.apache.ibatis.annotations.*;
import org.spring.springboot.domain.City;


@Mapper
public interface CityMapper {

    /**
     * 根据城市名称，查询城市信息
     *
     * @param cityName 城市名
     */
    @Select("SELECT * FROM city WHERE city_name=#{cityName}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "provinceId", column = "province_id"),
            @Result(property = "cityName", column = "city_name"),
            @Result(property = "description", column = "description"),
    })
    List<City> findByName(@Param("cityName") String cityName);

}
