package org.spring.springboot.mapper;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.*;
import org.spring.springboot.domain.Menu;
import org.spring.springboot.param.MenuSearchParam;

@Mapper
public interface MenuMapper {

    @Insert("INSERT INTO menu (name,url,parent_id,`index`,`create_time`) values (#{name},#{url},#{parentId},#{index},#{createTime})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int save(Menu menu);
    
    @Update({ "UPDATE menu SET name = #{name},url = #{url},parent_id= #{parentId} WHERE id = #{id}" })
    int update(Menu menu);
	
    @Delete({ "DELETE FROM menu  WHERE id = #{id}" })
    int deleteById(Long id);
    
    @Select("SELECT * FROM menu WHERE id = #{id}")
    @Results({
    	@Result(property = "id", column = "id"),
        @Result(property = "name", column = "name"),
        @Result(property = "url", column = "url"),
        @Result(property = "parentId", column = "parent_id"),
        @Result(property = "index", column = "index"),
        @Result(property = "createTime", column = "create_time",javaType=Date.class),
    })
    Menu findById(@Param("id")Long id);
    
    @Select("SELECT * FROM menu")
    @Results({
    	@Result(property = "id", column = "id"),
        @Result(property = "name", column = "name"),
        @Result(property = "url", column = "url"),
        @Result(property = "parentId", column = "parent_id"),
        @Result(property = "index", column = "index"),
        @Result(property = "createTime", column = "create_time",javaType=Date.class),   
    })
    Menu findByName(@Param("name") String name);
    
    @Select("SELECT * FROM menu WHERE id=#{id}")
    @Results({
    	@Result(property = "id", column = "id"),
        @Result(property = "name", column = "name"),
        @Result(property = "url", column = "url"),
        @Result(property = "parentId", column = "parent_id"),
        @Result(property = "index", column = "index"),
        @Result(property = "createTime", column = "create_time",javaType=Date.class),
    })
    Menu selectById(@Param("id") Long id);

    @SelectProvider(type = MenuProvider.class , method = "getList")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "url", column = "url"),
            @Result(property = "parentId", column = "parent_id"),            
            @Result(property = "parentMenu", column = "parent_id",one =@One(select="org.spring.springboot.mapper.MenuMapper.selectById")),            
            @Result(property = "index", column = "index"),
            @Result(property = "createTime", column = "create_time",javaType=Date.class),
    })
	List<Menu> getList(@Param("t")MenuSearchParam param);
    
    @Select("SELECT * FROM menu WHERE parent_id=#{parentId} ORDER BY `index`")
    @Results({
    	@Result(property = "id", column = "id"),
        @Result(property = "name", column = "name"),
        @Result(property = "url", column = "url"),
        @Result(property = "parentId", column = "parent_id"),
        @Result(property = "index", column = "index"),
        @Result(property = "createTime", column = "create_time",javaType=Date.class),
    })
    Menu selectByParentId(@Param("parentId") Long parentId);
    
    @SelectProvider(type = MenuProvider.class , method = "getSubList")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "url", column = "url"),
            @Result(property = "parentId", column = "parent_id"),            
            @Result(property = "subList", column = "id",many =@Many(select="org.spring.springboot.mapper.MenuMapper.selectByParentId")),
            @Result(property = "index", column = "index"),
            @Result(property = "createTime", column = "create_time",javaType=Date.class),
    })
	List<Menu> getSubList();

	
	
}
