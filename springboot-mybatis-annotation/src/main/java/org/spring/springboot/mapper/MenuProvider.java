package org.spring.springboot.mapper;

import org.apache.ibatis.annotations.Param;
import org.spring.springboot.param.MenuSearchParam;
import org.springframework.util.StringUtils;

public class MenuProvider {
 
    private final static String SQL ="SELECT id,name,url,parent_id,`index`,`create_time` FROM `menu` as t WHERE 1=1 ";

    public String getList(@Param("t")MenuSearchParam param){
        StringBuilder sb = new StringBuilder(SQL);      
        if(param==null) {
        	return sb.toString();
        }
        if(!StringUtils.isEmpty(param.getName())){
            sb.append(" and name like '")
            .append(param.getName())
            .append("%' ");          
        }
        if( param.getId()!= null){
            sb.append(" and id = #{t.id} ");
        }        
        if( param.getParentId()!= null){
            sb.append(" and parent_id = #{t.parentId} ");
        }        
        return sb.toString();
    }
    
    public String getSubList(){
        StringBuilder sb = new StringBuilder(SQL);    
        sb.append(" and parent_id is  null ");
        sb.append(" ORDER BY `index` ");
        return sb.toString();
    }
}
