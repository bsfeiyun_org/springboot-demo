package org.spring.springboot.param;

import javax.validation.constraints.NotBlank;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("菜单新增参数对象")
public class MenuUpdParam{

	@ApiModelProperty("菜单编号")	
	private Long id;
	@ApiModelProperty(value = "名称", name = "name")
	@NotBlank(message = "菜单名称不能为空")
    private String name;
	@ApiModelProperty(value = "路径", name = "url")
    private String url;
	@ApiModelProperty(value = "菜单父节点编号", name = "parentId")
    private Long parentId;	
	@ApiModelProperty("排序")	
    private Integer index;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}
    
}
