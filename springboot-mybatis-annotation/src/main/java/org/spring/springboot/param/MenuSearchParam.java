package org.spring.springboot.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("菜单查询参数对象")
public class MenuSearchParam{

	@ApiModelProperty("菜单编号")
	private Long id;
	@ApiModelProperty("名称")
    private String name;
	@ApiModelProperty("菜单父节点编号")
    private Long parentId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
    

}
