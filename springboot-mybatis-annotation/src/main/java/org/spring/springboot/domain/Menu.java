package org.spring.springboot.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel("菜单实体类")
public class Menu implements Serializable{

	private static final long serialVersionUID = -7082114404115584299L;
	@ApiModelProperty("菜单编号")
    private Long id;
	@ApiModelProperty("菜单名称")
    private String name;
	@ApiModelProperty("菜单路径")
    private String url;
	@ApiModelProperty("父菜单编号")
    private Long parentId;
	@ApiModelProperty("排序")
    private Integer index;
	@ApiModelProperty("创建时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")	
    private Date createTime;    
	@ApiModelProperty("父菜单")
    private Menu parentMenu;
	@ApiModelProperty("子菜单")
    private List<Menu> subList;
	
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Menu getParentMenu() {
		return parentMenu;
	}

	public void setParentMenu(Menu parentMenu) {
		this.parentMenu = parentMenu;
	}

	public List<Menu> getSubList() {
		if(null==subList) {
			return new ArrayList<Menu>();
		}
		return subList;
	}

	public void setSubList(List<Menu> subList) {
		this.subList = subList;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
