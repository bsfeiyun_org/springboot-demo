## 前言

学习总结，智慧的结晶，交流技术，不足之处请斧正。

springboot strategy 策略模式去掉 if else demo

## 技术选型

| 技术                   | 版本   | 说明                                    |
| ---------------------- | ------ | --------------------------------------- |
| Spring Boot            | 2.3.2  | MVC核心框架                             |
| MyBatis                | 3.5.5  | ORM框架                                 |
| hikari                 | 3.4.5  | 最快数据库连接池                            |



## 入口地址

http://localhost:8080

## 接口地址
org.spring.springboot.controller


## 你的点赞鼓励，是我们前进的动力~
