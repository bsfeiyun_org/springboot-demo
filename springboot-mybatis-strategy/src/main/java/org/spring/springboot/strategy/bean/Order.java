package org.spring.springboot.strategy.bean;

import org.spring.springboot.domain.City;

import lombok.Data;

@Data
public class Order {
	public static final int DISCOUT = 1;
	public static final int FREE = 2;
	public static final int HALF = 3;

	private int id;
	private String name;
	private OrderTypeEnum type;
	
	private City city;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public OrderTypeEnum getType() {
		return type;
	}

	public void setType(OrderTypeEnum type) {
		this.type = type;
	}
}