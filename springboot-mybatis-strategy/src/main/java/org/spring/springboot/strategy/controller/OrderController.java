package org.spring.springboot.strategy.controller;

import org.spring.springboot.strategy.bean.Order;
import org.spring.springboot.strategy.bean.OrderTypeEnum;
import org.spring.springboot.strategy.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.sf.json.JSONObject;

@RestController
@RequestMapping("/api/order")
public class OrderController {
	@Autowired
	private OrderService orderService;

	@GetMapping("/handler")
	public JSONObject handleOrder(@RequestParam OrderTypeEnum type, @RequestParam Long cityId) {
		Order order = new Order();
		order.setId(1);
		order.setName("免费订单");
		order.setType(type);
		Object data = orderService.handleOrder(order, cityId);
		JSONObject json = new JSONObject();
		json.put("data", data);
		return json;
	}
}