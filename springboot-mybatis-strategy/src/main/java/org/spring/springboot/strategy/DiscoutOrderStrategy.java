package org.spring.springboot.strategy;

import org.spring.springboot.domain.City;
import org.spring.springboot.service.CityService;
import org.spring.springboot.strategy.bean.Order;
import org.spring.springboot.strategy.handler.HandlerOrderType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@HandlerOrderType(Order.DISCOUT)
public class DiscoutOrderStrategy implements OrderStrategy {

	@Autowired
	private CityService cityService;

	@Override
	public Order handleOrder(Order order, Long cityId) {		
		order.setName("----处理打折订单----");
		City city = cityService.findCityById(cityId);
		order.setCity(city);
		return order;
	}
}