package org.spring.springboot.strategy.service;

import org.spring.springboot.strategy.bean.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderService {
	Order handleOrder(Order order,Long cityId);
}