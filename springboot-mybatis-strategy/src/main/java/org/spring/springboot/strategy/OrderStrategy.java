package org.spring.springboot.strategy;

import org.spring.springboot.strategy.bean.Order;

/**
 * 处理订单策略
 */
public interface OrderStrategy {

	Order handleOrder(Order order, Long cityId);
}