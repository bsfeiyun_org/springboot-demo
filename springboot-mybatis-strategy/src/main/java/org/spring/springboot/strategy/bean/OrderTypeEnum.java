package org.spring.springboot.strategy.bean;

public enum OrderTypeEnum {

	DISCOUT("打折", 1), FREE("免费", 2), HALF("半价", 3),;

	private String msg;
	private Integer code;

	OrderTypeEnum(String msg, Integer code) {
		this.msg = msg;
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public Integer getCode() {
		return code;
	}
}