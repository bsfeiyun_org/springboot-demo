package org.spring.springboot.strategy.service;

import org.spring.springboot.strategy.OrderStrategy;
import org.spring.springboot.strategy.bean.Order;
import org.spring.springboot.strategy.handler.HandlerOrderContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderServiceImpl implements OrderService {
	@Autowired
	HandlerOrderContext handlerOrderContext;

	@Override
	public Order handleOrder(Order order, Long cityId) {
		// 使用策略处理订单
		OrderStrategy orderStrategy = handlerOrderContext.getOrderStrategy(order.getType());
		return orderStrategy.handleOrder(order, cityId);
	}
}