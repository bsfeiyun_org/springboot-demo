# springboot-demo

#### 介绍
springboot2.3.2基于开源项目归纳学习总结



## 技术选型

| 技术                   | 版本   | 说明                                    |
| ---------------------- | ------ | --------------------------------------- |
| Spring Boot            | 2.3.2  | MVC核心框架                             |
| MyBatis                | 3.5.5  | ORM框架                                 |
| hikari                 | 3.4.5  | 最快数据库连接池                            |
| JDK                 | 1.8  | 开发环境版本                           |



## 模块说明

| 项目                   |  说明                                    |
| ---------------------- | --------------------------------------- |
| springboot-dubbo-client            | dubbo消费者                                 |
| springboot-dubbo-server            | dubbo生产者                                 |
| springboot-mybatis                 | mybatis xml版                            |
| springboot-mybatis-annotation      | mybatis 注解版                            |
| springboot-mybatis-strategy        | 策略模式去掉 if else |
| springboot-minio        | 轻量级文件存储 |




## 学习总结，智慧的结晶，交流技术，不足之处请斧正。

## 你的点赞鼓励，是我们前进的动力~
