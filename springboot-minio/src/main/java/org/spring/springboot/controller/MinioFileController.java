package org.spring.springboot.controller;

import java.io.InputStream;
import java.net.URLEncoder;
import javax.servlet.http.HttpServletResponse;
import org.spring.springboot.config.MinioProperties;
import org.spring.springboot.exception.DemoException;
import org.spring.springboot.res.ResultData;
import org.spring.springboot.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import io.minio.MinioClient;
import io.minio.ObjectStat;
import io.minio.PutObjectOptions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;

@SuppressWarnings("deprecation")
@Api(tags = { "文件存储接口" })
@RestController
@RequestMapping("api/minio/file")
public class MinioFileController {

    @Autowired
    private MinioClient minioClient;

    @Autowired
    private MinioProperties minioProperties;

    @ApiOperation(value = "下载文件")
    @GetMapping(value = "/download")
    @SneakyThrows(Exception.class)
    public void download(@RequestParam("fileName") String fileName, HttpServletResponse response) {
        InputStream in = null;
        final ObjectStat stat = minioClient.statObject(minioProperties.getBucketName(), fileName);
        response.setContentType(stat.contentType());
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        in = minioClient.getObject(minioProperties.getBucketName(), fileName);
        //IOUtils.copy(in, response.getOutputStream());        
       // response.setContentLength();
        StrUtil.stream(in, response.getOutputStream());
        in.close();
    }
    
    @ApiOperation(value = "上传文件")
    @PostMapping(value = "/upload")    
    public ResultData upload(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new DemoException("上传文件不能为空");
        } else {
            // 得到文件流
            final InputStream is = file.getInputStream();
            // 文件名
            final String fileName = file.getOriginalFilename();
            // 把文件放到minio的boots桶里面
            minioClient.putObject(minioProperties.getBucketName(), fileName, is, new PutObjectOptions(is.available(), -1));
            // 关闭输入流
            is.close();            
            return new ResultData(getUrl(minioProperties.getBucketName(),fileName));
        }
    }
    //查询公共链接 需设置文件夹权限读写权限
    public  String  getUrl(String bucketName,String  fileName){
        try {
            String url = minioClient.getObjectUrl(bucketName, fileName);
            return url;
        }catch (Exception e){
            return "获取失败";
        }
    }

    //查询私有链接
    public  String  getSignUrl(String bucketName,String  fileName){
        try {
            String url = minioClient.presignedGetObject(bucketName, fileName);
            return url;
        }catch (Exception e){
            return "获取失败";
        }
    }

    @ApiOperation(value = "删除文件")
    @GetMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @SneakyThrows(Exception.class)
    public ResultData delete(@RequestParam("fileName") String fileName) {
        minioClient.removeObject(minioProperties.getBucketName(), fileName);
        return new ResultData("");
    }

}