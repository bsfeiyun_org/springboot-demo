package org.spring.springboot.res;

import lombok.Getter;

@Getter
public class ResultData {
    /**
     * 状态码
     */
    private int code;
    /**
     * 响应信息，用来说明响应情况
     */
    private String msg;
    /**
     * 响应的具体数据
     */
    private Object data;

    public ResultData(Object data) {
        this(ResultCode.SUCCESS, data);
    }

    public ResultData(ResultCode resultCode, Object data) {
        this.code = resultCode.getCode();
        this.msg = resultCode.getMsg();
        this.data = data;
    }
    
    public ResultData(ResultCode resultCode, Object data,String msg) {
        this.code = resultCode.getCode();
        this.msg = msg;
        this.data = data;
    }
}