package org.spring.springboot.res;

import lombok.Getter;

@Getter
public enum ResultCode {

    SUCCESS(200, "成功"),

    FAILED(201, "响应失败"),
    VALIDATE_FAILED(400, "参数校验失败"),
    LOGIN_AGAIN(401, "重新登录"),
    ERROR(204, "未知错误"),
    
    PERMISSION_TOKEN_EXPIRED(1001, "token已过期"),
    PERMISSION_TOKEN_INVALID(1002, "无效的token"),
    PERMISSION_SIGNATURE_ERROR(1003,"签名错误"),
    USER_NOT_LOGGED_IN(1004,"请先登录"),
    ;

    private int code;
    private String msg;

    ResultCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}