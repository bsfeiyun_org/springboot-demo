package org.spring.springboot.exception;


import org.spring.springboot.res.ResultCode;

import lombok.Getter;

@Getter
public class APIException extends RuntimeException {

	private static final long serialVersionUID = -3056582710561410409L;
	private ResultCode code;
    private String msg;

    public APIException() {
        this(ResultCode.ERROR, "接口错误");
    }

    public APIException(ResultCode code) {
        this(code, code.getMsg());
    }

    public APIException(ResultCode code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }
}