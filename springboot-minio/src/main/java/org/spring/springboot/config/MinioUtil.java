package org.spring.springboot.config;

import org.spring.springboot.exception.DemoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.minio.MinioClient;
import lombok.extern.slf4j.Slf4j;

/**
 * minio客户端配置
 */
@Slf4j
@Configuration
public class MinioUtil {

    @Autowired
    private MinioProperties minioData;

    /**
     * 初始化minio客户端,不用每次都初始化
     * @return MinioClient
     * @throws DemoException 
     */
    @Bean
    public MinioClient minioClient(){
        try {
            return new MinioClient(minioData.getUrl(), minioData.getAccessKey(), minioData.getSecretKey());
        }
        catch (final Exception e) {
            log.error("初始化minio异常:{}", e.fillInStackTrace());
            return null;
        }
    }
}