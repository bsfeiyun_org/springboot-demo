package org.spring.springboot.dubbo;

import com.alibaba.dubbo.config.annotation.Reference;

import org.spring.springboot.domain.City;
import org.springframework.stereotype.Component;

/**
 * 城市 Dubbo 服务消费者 
 */
@Component
public class CityDubboConsumerService {

    //@Reference(version = "1.0.0")
    @Reference
    private CityDubboService cityDubboService;

    public City printCity() {
        String cityName="陇西";
        City city = cityDubboService.findCityByName(cityName);
        System.out.println(city.toString());
        return city;
    }
}
