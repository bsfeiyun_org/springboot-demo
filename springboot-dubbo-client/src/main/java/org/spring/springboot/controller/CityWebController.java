package org.spring.springboot.controller;


import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.spring.springboot.dubbo.CityDubboConsumerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value = "api")
public class CityWebController {

	@Resource
	private CityDubboConsumerService cityService;

	//http://localhost:8082/api/city/info
	@RequestMapping(method = RequestMethod.POST, value = "/city/info")
	@ResponseBody
	public JSONObject getEcard(@RequestBody JSONObject param) {
		JSONObject json = new JSONObject();		
		// 姓名
		String name = null;
		if (param.containsKey("name")) {
			name = param.getString("name");
		}
		json.put("value",cityService.printCity());		
		return json;
	}
}
